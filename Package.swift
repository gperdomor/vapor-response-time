// swift-tools-version:4.1

import PackageDescription

let package = Package(
    name: "ResponseTime",
    products: [
        .library(name: "ResponseTime", targets: ["ResponseTime"]),
    ],
    dependencies: [
         // 💧 A server-side Swift web framework.
        .package(url: "https://github.com/vapor/vapor.git", from: "3.0.0"),
    ],
    targets: [
        .target(name: "ResponseTime", dependencies: ["Vapor"]),
        .testTarget(name: "ResponseTimeTests", dependencies: ["ResponseTime"])
    ]
)
