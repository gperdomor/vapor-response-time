//
//  ResponseTimeMiddlewareTests.swift
//  Response Time
//
//  Created by Gustavo Perdomo on 08/29/18.
//  Copyright © 2018 Gustavo Perdomo. All rights reserved.

@testable import ResponseTime

import Vapor
import XCTest

final class ResponseTimeMiddlewareTests: XCTestCase {
    func testDefaultConfig() throws {
        let config = ResponseTimeMiddleware.Configuration.default()

        XCTAssertEqual(config.header, "X-Response-Time")
        XCTAssertEqual(config.digits, 3)
        XCTAssertEqual(config.suffix, true)
        XCTAssertEqual(config.format, "%.3f ms")
    }

    func testConfig() throws {
        let config = ResponseTimeMiddleware.Configuration(header: "custom-header", digits: 5, suffix: false)

        XCTAssertEqual(config.header, "custom-header")
        XCTAssertEqual(config.digits, 5)
        XCTAssertEqual(config.suffix, false)
        XCTAssertEqual(config.format, "%.5f")
    }

    func testRespond() throws {
        let responseTime = ResponseTimeMiddleware()
        var services = Services.default()
        var middlewareConfig = MiddlewareConfig()
        middlewareConfig.use(responseTime)
        services.register(middlewareConfig)

        let app = try Application(services: services)

        let req = Request(using: app)

        let response = try app.make(Responder.self).respond(to: req).wait()

        XCTAssertTrue(response.http.headers.contains(name: "X-Response-Time"))
    }

    // MARK: Linux Helper

    /// Check Linux Tests
    func testLinuxTestSuiteIncludesAllTests() throws {
        #if os(macOS) || os(iOS) || os(tvOS) || os(watchOS)
        let thisClass = type(of: self)
        let linuxCount = thisClass.allTests.count
        let darwinCount = Int(thisClass.defaultTestSuite.testCaseCount)
        XCTAssertEqual(linuxCount, darwinCount, "\(darwinCount - linuxCount) tests are missing from allTests")
        #endif
    }

    static var allTests = [
        ("testLinuxTestSuiteIncludesAllTests", testLinuxTestSuiteIncludesAllTests),
        ("testDefaultConfig", testDefaultConfig),
        ("testConfig", testConfig),
        ("testRespond", testRespond)
    ]
}
