//
//  XCTestManifests.swift
//  Response Time
//
//  Created by Gustavo Perdomo on 08/29/18.
//  Copyright © 2018 Gustavo Perdomo. All rights reserved.

import XCTest

#if !os(macOS)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(ResponseTimeMiddlewareTests.allTests)
    ]
}
#endif
