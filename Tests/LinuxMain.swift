//
//  LinuxMain.swift
//  Response Time
//
//  Created by Gustavo Perdomo on 08/29/18.
//  Copyright © 2018 Gustavo Perdomo. All rights reserved.

import XCTest

import ResponseTimeTests

var tests = [XCTestCaseEntry]()

tests += ResponseTimeTests.allTests()

XCTMain(tests)
