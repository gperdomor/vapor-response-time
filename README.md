# Response Time

Adds response time header for Vapor 3 responses.

## Installation

Add this project to the `Package.swift` dependencies of your Vapor project:

```swift
.package(url: "https://gitlab.com/gperdomor/vapor-response-time.git", from: "1.0.0")
```

## Setup

Just import the package

```swift
import ResponseTime
```

and add the middleware to the middleware config of your application

```swift
var middlewareConfig = MiddlewareConfig()
middlewareConfig.use(ResponseTimeMiddleware())
// Other middlewares
services.register(middlewareConfig)
```

**Note:** You should ensure you set the `ResponseTimeMiddleware` as the first middleware in your `MiddlewareConfig`.

### Customize

You can override the default configuration to change the header name, number of decimals or the presence of suffis for the response time. Example:

```swift
let rtConfig = ResponseTimeMiddleware.Configuration(
    header: "my-custom-response-time",
    digits: 5,
    suffix: false
)

var middlewareConfig = MiddlewareConfig()
middlewareConfig.use(ResponseTimeMiddleware(configuration: rtConfig))
// Other middlewares
services.register(middlewareConfig)
```

| Parameter |     Default     | Description                                                                |
| --------- | :-------------: | -------------------------------------------------------------------------- |
| header    | X-Response-Time | The header name                                                            |
| digits    |        3        | Number of float digits of the response time value                          |
| suffix    |      true       | Include or exclude the milliseconds abbr (`ms`) to the response time value |

## Credits

This package is developed and maintained by [Gustavo Perdomo](https://gitlab.com/gperdomor).

## License

ResponseTime is released under the [MIT License](LICENSE).
