//
//  ResponseTimeMiddleware.swift
//  Response Time
//
//  Created by Gustavo Perdomo on 08/29/18.
//  Copyright © 2018 Gustavo Perdomo. All rights reserved.

import Vapor

/// Middleware that adds the response time in request responses.
/// For configuration of this middleware please use the `ResponseTimeMiddleware.Configuration` object.
///
/// - note: Make sure this middleware is inserted before all your error/abort middlewares,
///         so that even the failed request responses contain proper response time information.
public final class ResponseTimeMiddleware: Middleware {
    /// Configuration used for populating headers in responses.
    public struct Configuration {
        /// Default configuration.
        ///
        /// - Header: X-Response-Time.
        /// - Digits: 3
        /// - Suffix: `ms`
        public static func `default`() -> Configuration {
            return .init(
                header: "X-Response-Time",
                digits: 3,
                suffix: true
            )
        }

        /// Header name for the header response.
        public let header: String

        /// If set to yes, the time unit will be sent with the response time value.
        public let suffix: Bool

        /// Number of decimal digits in the response time value.
        public let digits: Int

        public let format: String

        /// Instantiate a ResponseTimeConfiguration struct that can be used to create a `ResponseTimeConfiguration`
        /// middleware for adding support for Response Time in your responses.
        ///
        /// - parameters:
        ///   - header: The header name which will contains the response time.
        ///   - digits: The number of decimal digits for the response value.
        ///   - suffix: If true, the time units will be included with the response time value.
        public init(
            header: String,
            digits: Int,
            suffix: Bool
            ) {
            self.header = header
            self.digits = digits
            self.suffix = suffix

            self.format = "%.\(digits)f" + (suffix ? " ms" : "")
        }
    }

    /// Configuration used for populating headers in response.
    public let configuration: Configuration

    /// Creates a Response Time middleware with the specified configuration.
    ///
    /// - parameters:
    ///     - configuration: Configuration used for populating headers in
    ///                      response.
    public init(configuration: Configuration = .default()) {
        self.configuration = configuration
    }

    public func respond(to request: Request, chainingTo next: Responder) throws -> EventLoopFuture<Response> {
        let date = Date()

        let response = try next.respond(to: request)

        return response.map { res in
            let responseTime = Date().timeIntervalSince(date) * 1000

            res.http.headers.replaceOrAdd(
                name: self.configuration.header,
                value: String(format: self.configuration.format, responseTime)
            )

            return res
        }
    }
}
